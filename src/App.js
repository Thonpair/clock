import './App.css';
import Hour from './Hour';

function App() {
  return (
    <div className="App">
      <Hour />
    </div>
  );
}

export default App;
