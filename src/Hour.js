import { useState, useEffect } from "react";
import "./Hour.css";

export default function Hour(){
    const [value, setValue] = useState(
        new Date().toLocaleTimeString()
        );
    useEffect(()=> {
        const interval = setInterval(() => setValue(
            new Date().toLocaleTimeString(), 1000)
            );
            document.title = value;
        return () => {
            clearInterval(interval)
        }
    });
    return (
        <div className="hour"> 
            {value}
        </div>
    )
}